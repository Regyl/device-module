package liga.medical.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeviceModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeviceModuleApplication.class, args);
    }
    
}
