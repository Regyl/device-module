package liga.medical.core.controller.rest;


import liga.medical.DeviceIdentificationDto;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/devices/")
public class DeviceController {

    private final AmqpTemplate amqpTemplate;

    public DeviceController(AmqpTemplate amqpTemplate) {
        this.amqpTemplate = amqpTemplate;
    }

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void doSend(@RequestParam DeviceIdentificationDto dto) {
        amqpTemplate.convertAndSend("medical", dto);
    }
}
